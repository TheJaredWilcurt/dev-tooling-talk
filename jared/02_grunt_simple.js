module.exports = function (grunt) {
    grunt.initConfig({
        sass: {
            dist: {
                options: {
                    sourcemap: 'dist/css/style.map'
                },
                files: [{
                    expand: true,
                    cwd: 'styles',
                    src: 'src/sass/style.scss',
                    dest: 'dist/css/style.css',
                    ext: '.css'
                }]
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-sass');

    grunt.registerTask('default', ['sass']);
}

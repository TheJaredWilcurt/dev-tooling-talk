var fs = require('fs');
var sass = require('node-sass');

sass.render({
    'file': 'src/sass/style.scss',
    'outfile': 'dist/css/style.map',
    'outputStyle': 'expanded',
    'indentedSyntax': true,
    'sourceComments': true,
    'sourceMap': 'dist/css/style.map',
    'sourceMapContents': true
}, function (error, result) {
    if (error) {
        console.log(error);
    } else {
        fs.writeFile('dist/css/style.css', result.css.toString(), function (err) {
            if (err) {
                console.log(err);
            }
        });
        fs.writeFile('dist/css/style.map', result.map.toString(), function (err) {
            if (err) {
                console.log(err);
            }
        });
    }
});

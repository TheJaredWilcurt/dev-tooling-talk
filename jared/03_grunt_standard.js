module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            dist: {
                options: {
                    sourcemap: 'dist/css/style.map'
                },
                files: [{
                    expand: true,
                    cwd: 'styles',
                    src: 'src/sass/style.scss',
                    dest: 'dist/css/style.css',
                    ext: '.css'
                }]
            }
        },
        concat: {
            options: {
                separator: ';\n'
            },
            dist: {
                src: [
                    'src/js/script.js'
                ],
                dest: 'dist/js/script.concat.js'
            }
        },
        uglify: { // Begin JS Minify Plugin
            build: {
                src: 'dist/js/script.concat.js',
                dest: 'dist/js/script.min.js'
            }
        },
        watch: {
            grunt: {
                options: {
                    reload: true
                },
                files: ['Gruntfile.js']
            },
            css: {
                files: ['src/sass/style.scss'],
                tasks: ['sass']
            },
            js: {
                files: ['src/js/*.js', 'src/js/**/*.js']
                tasks: ['concat, uglify']
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['watch']);
}

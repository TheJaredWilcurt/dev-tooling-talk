var animalList = require('./animals');

var randomAnimal = function () {
	var num = Math.floor(Math.random() * animalList.length);
	return animalList[num];
}

module.exports = randomAnimal;
